const functions = require("firebase-functions");
const axios = require("axios");
const cors = require("cors")({
  origin: true,
});

const googleMapsApiKey = "YOUR_API_KEY";

exports.searchGooglePlaces = functions.https.onRequest(async (req, res) => {
  try {
    const searchTerm = encodeURI(
      `${req.body.businessName} ${req.body.zipCode}`
    );

    const { data } = await axios.get(
      `https://maps.googleapis.com/maps/api/place/textsearch/json?query=${searchTerm}&key=${googleMapsApiKey}`
    );

    let searchResults = [];
    if (data.status === "OK") {
      data.results.forEach((result) => {
        searchResults.push({
          placeId: result.place_id,
          name: result.name,
          address: result.formatted_address,
        });
      });
    }

    return cors(req, res, () => {
      res.status(200).send(searchResults);
    });
  } catch (error) {
    return cors(req, res, () => {
      res.status(500).send();
    });
  }
});

exports.getBusinessDetailsFromGoogle = functions.https.onRequest(
  async (req, res) => {
    try {
      const placeId = req.body.placeId;

      let { data } = await axios.get(
        `https://maps.googleapis.com/maps/api/place/details/json?placeid=${placeId}&key=${googleMapsApiKey}`
      );

      if (data.status === "OK") {
        let result = data.result;
        let splitAddress = result.formatted_address.split(",");
        let stateZip = splitAddress[2].trim().split(" ");
        var hours = {};

        if (result.opening_hours) {
          result.opening_hours.periods.forEach((h) => {
            var open = h.open;
            var close = h.close;
            var openTime = `${open.time.substring(0, 2)}:${open.time.substring(
              2,
              4
            )}`;
            var closeTime = `${close.time.substring(
              0,
              2
            )}:${close.time.substring(2, 4)}`;
            switch (open.day) {
              case 0:
                hours["Sunday"] = {
                  isOpen: true,
                  from: openTime,
                  to: closeTime,
                };
                break;
              case 1:
                hours["Monday"] = {
                  isOpen: true,
                  from: openTime,
                  to: closeTime,
                };
                break;
              case 2:
                hours["Tuesday"] = {
                  isOpen: true,
                  from: openTime,
                  to: closeTime,
                };
                break;
              case 3:
                hours["Wednesday"] = {
                  isOpen: true,
                  from: openTime,
                  to: closeTime,
                };
                break;
              case 4:
                hours["Thursday"] = {
                  isOpen: true,
                  from: openTime,
                  to: closeTime,
                };
                break;
              case 5:
                hours["Friday"] = {
                  isOpen: true,
                  from: openTime,
                  to: closeTime,
                };
                break;
              case 6:
                hours["Saturday"] = {
                  isOpen: true,
                  from: openTime,
                  to: closeTime,
                };
                break;
              default:
                break;
            }
          });
        }

        var objGoogleData = {
          name: result.name,
          streetAddress1: splitAddress[0],
          city: splitAddress[1],
          state: stateZip[0],
          zipCode: stateZip[1],
          geoCode: result.geometry.location,
          phoneNumber: result.formatted_phone_number,
          hours: hours,
          link: result.website,
        };

        return cors(req, res, () => {
          res.status(200).send(objGoogleData);
        });
      }

      return cors(req, res, () => {
        res.status(404).send();
      });
    } catch (error) {
      return cors(req, res, () => {
        res.status(500).send();
      });
    }
  }
);
